﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace HrDepartment.Domain.Entities
{
	/// <summary>
	/// Сотрудник
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class Employee
	{
		public int Id { get; set; }
		public int JobSeekerId { get; set; }
		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public DateTime BirthDate { get; set; }
	}
}
