﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace HrDepartment.Utils.Extensions
{
	[ExcludeFromCodeCoverage]
	public static class SerializationExtensions
	{
		public static string ToJson<T>(this T obj)
		{
			return JsonConvert.SerializeObject(obj, Formatting.None);
		}
	}
}
