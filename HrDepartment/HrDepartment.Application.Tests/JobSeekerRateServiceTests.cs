using System;
using NUnit.Framework;
using Moq;
using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using System.Threading.Tasks;
using System.Reflection;

namespace HrDepartment.Application.Tests
{
    [TestFixture]
    public class JobSeekerRateServiceTests
    {
        private Mock<ISanctionService> _sanctionServiceMock;

        private readonly Type _jobSeekerRateServiceType = typeof(JobSeekerRateService);

        [SetUp]
        public void SetUp()
        {
            _sanctionServiceMock = new Mock<ISanctionService>();
        }

        private JobSeekerRateService GetJobSeekerRateService()
        {
            return new JobSeekerRateService(_sanctionServiceMock.Object);
        }

        [Test]
        public void Ctor_SanctionServiceIsNull_ThrowsArgumentNullException()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(null));
            //Assert.AreEqual("sanctionService", exception.ParamName);
        }

        [Test]
        public async Task CalculateJobSeekerRatingAsync_IsInSanctionsList_Return0()
        {
            var jobSeeker = new JobSeeker()
            {
                LastName = "Smith",
                FirstName = "John",
                MiddleName = "Peter",
                BirthDate = new DateTime(2000, 1, 1)
            };

            _sanctionServiceMock.Setup(s => s.IsInSanctionsListAsync(jobSeeker.LastName, jobSeeker.FirstName, jobSeeker.MiddleName, jobSeeker.BirthDate))
                .ReturnsAsync(true);

            var jobSeekerRateService = GetJobSeekerRateService();

            var result = await jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);

            Assert.AreEqual(0, result);
        }

        [TestCase("2004, 1, 1", EducationLevel.None, 0.5, BadHabits.Drugs, ExpectedResult = 0)]
        [TestCase("2004, 2, 3", EducationLevel.School, 1, BadHabits.Alcoholism, ExpectedResult = 10)]
        [TestCase("1996, 3, 4", EducationLevel.College, 3, BadHabits.Smoking, ExpectedResult = 45)]
        [TestCase("1990, 4, 5", EducationLevel.College, 5, BadHabits.None, ExpectedResult = 60)]
        [TestCase("1990, 5, 6", EducationLevel.University, 10, BadHabits.Smoking, ExpectedResult = 90)]
        [TestCase("1957, 6, 7", EducationLevel.University, 40, BadHabits.Smoking, ExpectedResult = 90)]
        [TestCase("1956, 7, 8", EducationLevel.University, 40, BadHabits.Smoking, ExpectedResult = 80)]
        [TestCase("1985, 8, 9", EducationLevel.University, 20, BadHabits.None, ExpectedResult = 95)]
        [TestCase("2003, 9, 10", EducationLevel.School, 0.5, BadHabits.None, ExpectedResult = 30)]
        public async Task<int> CalculateJobSeekerRatingAsyncTests(DateTime birthDate, EducationLevel educationLevel, double workExperienceInYears, BadHabits badHabits)
        {
            var jobSeeker = new JobSeeker()
            {
                BirthDate = birthDate,
                Education = educationLevel,
                Experience = workExperienceInYears,
                BadHabits = badHabits
            };

            var jobSeekerRateService = GetJobSeekerRateService();

            return await jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeeker);
        }

        [TestCase("2004, 1, 1")]
        [TestCase("2005, 2, 3")]
        [TestCase("1955, 1, 1")]
        [TestCase("1956, 4, 5")]
        public void CalculateBirthDateRating_AgeLessThan_18_or_AgeMoreOrEqual_65_Return_0(DateTime birthDate)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateBirthDateRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { birthDate });

            Assert.AreEqual(0, result);
        }

        [TestCase("2003, 1, 1")]
        [TestCase("2000, 2, 3")]
        [TestCase("1957, 4, 5")]
        public void CalculateBirthDateRating_AgeMoreOrEqual_18_and_LessThan_65_Return_10(DateTime birthDate)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateBirthDateRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { birthDate });

            Assert.AreEqual(10, result);
        }

        [TestCase(EducationLevel.None, ExpectedResult = 0)]
        [TestCase(EducationLevel.School, ExpectedResult = 5)]
        [TestCase(EducationLevel.College, ExpectedResult = 15)]
        [TestCase(EducationLevel.University, ExpectedResult = 35)]
        public int CalculateEducationRatingTests(EducationLevel educationLevel)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateEducationRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { educationLevel });

            return (int)result;
        }

        [TestCase(int.MinValue)]
        [TestCase(int.MaxValue)]
        [TestCase(4)]
        public void CalculateEducationRating_ThrowsEducationLevelOutOfRangeException(EducationLevel educationLevel)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateEducationRating", BindingFlags.NonPublic | BindingFlags.Instance);

            var exception = Assert.Throws<TargetInvocationException>(() => method.Invoke(jobSeekerRateService, new object[] { educationLevel }));
            Assert.That(exception.InnerException, Is.TypeOf<ArgumentOutOfRangeException>());
        }

        [TestCase(0.1)]
        [TestCase(0.5)]
        public void CalculateExperienceRating_workExperienceInYearsLessThan_1_Return_5(double workExperienceInYears)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateExperienceRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { workExperienceInYears });

            Assert.AreEqual(5, result);
        }

        [TestCase(1)]
        [TestCase(2.9)]
        public void CalculateExperienceRating_workExperienceInYearsMoreOrEqual1_and_LessThan_3_Return_10(double workExperienceInYears)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateExperienceRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { workExperienceInYears });

            Assert.AreEqual(10, result);
        }

        [TestCase(3)]
        [TestCase(4.9)]
        public void CalculateExperienceRating_workExperienceInYearsMoreOrEqual3_and_LessThan_5_Return_15(double workExperienceInYears)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateExperienceRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { workExperienceInYears });

            Assert.AreEqual(15, result);
        }

        [TestCase(5)]
        [TestCase(9.9)]
        public void CalculateExperienceRating_workExperienceInYearsMoreOrEqual5_and_LessThan_10_Return_25(double workExperienceInYears)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateExperienceRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { workExperienceInYears });

            Assert.AreEqual(25, result);
        }

        [TestCase(10)]
        [TestCase(20.9)]
        public void CalculateExperienceRating_workExperienceInYearsMoreOrEqual10_Return_40(double workExperienceInYears)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateExperienceRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { workExperienceInYears });

            Assert.AreEqual(40, result);
        }

        [TestCase(BadHabits.None, ExpectedResult = 10)]
        [TestCase(BadHabits.Smoking, ExpectedResult = 5)]
        [TestCase(BadHabits.Alcoholism, ExpectedResult = -5)]
        [TestCase(BadHabits.Drugs, ExpectedResult = -35)]
        [TestCase(BadHabits.Smoking | BadHabits.Alcoholism, ExpectedResult = -10)]
        [TestCase(BadHabits.Smoking | BadHabits.Drugs, ExpectedResult = -40)]
        [TestCase(BadHabits.Smoking | BadHabits.Alcoholism | BadHabits.Drugs, ExpectedResult = -55)]
        [TestCase(BadHabits.Alcoholism | BadHabits.Drugs, ExpectedResult = -50)]
        public int CalculateHabitsRatingTests(BadHabits badHabits)
        {
            var jobSeekerRateService = GetJobSeekerRateService();

            var method = _jobSeekerRateServiceType.GetMethod("CalculateHabitsRating", BindingFlags.NonPublic | BindingFlags.Instance);
            var result = method.Invoke(jobSeekerRateService, new object[] { badHabits });

            return (int)result;
        }
    }
}