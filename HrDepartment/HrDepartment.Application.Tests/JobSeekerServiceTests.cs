﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Infrastructure.Interfaces;
using AutoMapper;
using HrDepartment.Application.Dto;
using HrDepartment.Domain.Enums;

namespace HrDepartment.Application.Tests
{
    [TestFixture]
    public class JobSeekerServiceTests
    {
        private Mock<JobSeeker> _jobSeekerRateService;

        private Mock<IStorage> _storageMock;
        private Mock<IRateService<JobSeeker>> _rateServiceMock;
        private Mock<INotificationService> _notificationServiceMock;
        private Mock<IMapper> _mapperMock;

        private Mock<ISanctionService> _sanctionServiceMock;

        private IStorage Storage => _storageMock.Object;
        private IRateService<JobSeeker> RateService => _rateServiceMock.Object;
        private INotificationService NotificationService => _notificationServiceMock.Object;
        private IMapper Mapper => _mapperMock.Object;

        [SetUp]
        public void SetUp()
        {
            _storageMock = new Mock<IStorage>();
            _rateServiceMock = new Mock<IRateService<JobSeeker>>();
            _notificationServiceMock = new Mock<INotificationService>();
            _mapperMock = new Mock<IMapper>();

            _sanctionServiceMock = new Mock<ISanctionService>();
    }

        private JobSeekerService GetJobSeekerService()
        {
            return new JobSeekerService(Storage, RateService, Mapper, NotificationService);
        }

        [Test]
        public async Task AddAsync_ShouldAddJobSeekerToDataBase()
        {
            var jobSeekerService = GetJobSeekerService();

            var lastName = "Smith";
            var firstName = "John";
            var middleName = "Peter";
            
            var jobSeeker = new JobSeeker()
            {
                LastName = lastName,
                FirstName = firstName,
                MiddleName = middleName,
            };

            var dto = new BackgroundInformationDto()
            {
                LastName = lastName,
                FirstName = firstName,
                MiddleName = middleName
            };

            _mapperMock.Setup(m => m.Map<JobSeeker>(dto)).Returns(jobSeeker);

            _storageMock.Setup(s => s.AddAsync(jobSeeker)).ReturnsAsync(jobSeeker);

            var result = await jobSeekerService.AddAsync(dto);

            Assert.AreEqual(jobSeeker.LastName, result.LastName);
            Assert.AreEqual(jobSeeker.FirstName, result.FirstName);
            Assert.AreEqual(jobSeeker.MiddleName, result.MiddleName);
        }

        [TestCase(30)]
        [TestCase(120)]
        public async Task RateJobSeekerAsync_RatingIsNot100_JustReturnRatingWithNoNotify(int expRating)
        {
            var jobSeekerService = GetJobSeekerService();
            
            var jobSeekerId = new Random().Next(1, 100);

            var jobSeeker = new JobSeeker()
            {
                Id = jobSeekerId
            };

            _storageMock.Setup(s => s.GetByIdAsync<JobSeeker, int>(jobSeekerId)).ReturnsAsync(jobSeeker);

            _rateServiceMock.Setup(r => r.CalculateJobSeekerRatingAsync(jobSeeker)).ReturnsAsync(expRating);

            var rating = await jobSeekerService.RateJobSeekerAsync(jobSeekerId);

            _notificationServiceMock.Verify(n => n.NotifyRockStarFoundAsync(jobSeeker), Times.Never);

            Assert.AreEqual(expRating, rating);
        }

        [Test]
        public async Task RateJobSeekerAsync_RatingIsEqual100_Notify()
        {
            var jobSeekerService = GetJobSeekerService();

            var expRating = 100;

            var jobSeekerId = new Random().Next(1, 100);

            var jobSeeker = new JobSeeker()
            {
                Id = jobSeekerId
            };

            _storageMock.Setup(s => s.GetByIdAsync<JobSeeker, int>(jobSeekerId)).ReturnsAsync(jobSeeker);

            _rateServiceMock.Setup(r => r.CalculateJobSeekerRatingAsync(jobSeeker)).ReturnsAsync(expRating);

            await jobSeekerService.RateJobSeekerAsync(jobSeekerId);

            _notificationServiceMock.Verify(n => n.NotifyRockStarFoundAsync(jobSeeker), Times.Once);
        }
    }
}