﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace HrDepartment.Infrastructure.Dto
{
	[ExcludeFromCodeCoverage]
	public class EmailMessageDto
	{
		public string From { get; set; }
		public IEnumerable<string> To { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
	}
}
