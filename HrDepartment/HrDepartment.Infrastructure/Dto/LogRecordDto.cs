﻿using System;
using System.Diagnostics.CodeAnalysis;
using HrDepartment.Infrastructure.Enums;

namespace HrDepartment.Infrastructure.Dto
{
	[ExcludeFromCodeCoverage]
	public class LogRecordDto
	{
		public LogLevel LogLevel { get; set; }
		public DateTime DateTime { get; set; }
		public string Message { get; set; }
		public Exception Exception { get; set; }
	}
}
