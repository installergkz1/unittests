﻿using HrDepartment.Infrastructure.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace HrDepartment.Infrastructure.Implementation
{
	[ExcludeFromCodeCoverage]
	public class ConstAppConfiguration : IAppConfiguration
	{
		public string AzureServiceBusConnectionString => "Fake connection string";
	}
}
